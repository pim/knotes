# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# G.Sora <g.sora@tiscali.it>, 2013, 2014, 2020, 2021, 2022, 2024.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-28 00:38+0000\n"
"PO-Revision-Date: 2024-05-29 12:40+0200\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: notesagentalarmdialog.cpp:44
#, kde-format
msgctxt "@title:window"
msgid "Alarm"
msgstr "Alarma"

#: notesagentalarmdialog.cpp:56
#, kde-format
msgctxt "@label:textbox"
msgid "The following notes triggered alarms:"
msgstr "Le notas sequente discatenava alarmas:"

#: notesagentalarmdialog.cpp:94
#, kde-format
msgid "Remove Alarm"
msgstr "Remove alarma"

#: notesagentalarmdialog.cpp:96
#, kde-format
msgctxt "@action"
msgid "Show Note..."
msgstr "Monstra nota ..."

#: notesagentalarmdialog.cpp:98
#, kde-format
msgctxt "@action"
msgid "Modify Alarm..."
msgstr "Modifica alarma..."

#: notesagentalarmdialog.cpp:152
#, kde-format
msgid "Are you sure to remove alarm?"
msgstr "Tu es secur que tu vole remover alarma?"

#: notesagentalarmdialog.cpp:153
#, kde-format
msgctxt "@title:window"
msgid "Remove Alarm"
msgstr "Remove alarma"

#: notesagentalarmdialog.cpp:218
#, kde-format
msgid "Error during fetch alarm info."
msgstr "Errr quandotu reportava info de alarma."

#: notesagentalarmdialog.cpp:218
#, kde-format
msgid "Alarm"
msgstr "Alarma"

#: notesagentsettingsdialog.cpp:33
#, kde-format
msgctxt "@title:window"
msgid "Configure Notes Agent"
msgstr "Configura agente de notas"

#: notesagentsettingsdialog.cpp:48
#, kde-format
msgid "Notify"
msgstr "Notifica"

#: notesagentsettingsdialog.cpp:50
#, kde-format
msgid "Network"
msgstr "Rete"

#: notesagentsettingsdialog.cpp:58
#, kde-format
msgid "Notes Agent"
msgstr "Agente de Notas"

#: notesagentsettingsdialog.cpp:60
#, kde-format
msgid "Notes Agent."
msgstr "Agente de Notas."

#: notesagentsettingsdialog.cpp:62
#, kde-format
msgid "Copyright (C) 2013-%1 Laurent Montel"
msgstr "Copyright (C) 2013-%1 Laurent Montel"

#: notesagentsettingsdialog.cpp:64
#, kde-format
msgctxt "@info:credit"
msgid "Laurent Montel"
msgstr "Laurent Montel"

#: notesagentsettingsdialog.cpp:64
#, kde-format
msgid "Maintainer"
msgstr "Mantenitor"

#: notesagentsettingsdialog.cpp:67
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Giovanni Sora"

#: notesagentsettingsdialog.cpp:67
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "g.sora@tiscali.it"

#: notesmanager.cpp:173
#, kde-format
msgid "Note Received"
msgstr "Nota recipite"
